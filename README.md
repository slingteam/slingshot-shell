# Slingshot Shell

Miscelaneous styling utilities

This project makes use of [Inuit's style namespaces](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/)
and [BEM](http://getbem.com/naming/).

## Installing

`npm i -D @slingshot/shell`

## Dependencies

[sass-mq](http://sass-mq.github.io/sass-mq/) for media query helpers and class generators
